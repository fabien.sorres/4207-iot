#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;

const int but = 8;
const int led1 = 3;
const int led2 = 6; 
 
int colorR = 255;
int colorG = 0;
int colorB = 0;

//changement d'état du bouton
boolean oldSwitchState = LOW;
boolean newSwitchState = LOW;

boolean LEDstatus = LOW;



/////////////FONCTIONS///////////////////

void monSysteme() {
      newSwitchState = digitalRead(but);

      if(newSwitchState != oldSwitchState) {
         if ( newSwitchState == HIGH )
       {
           if ( LEDstatus == LOW ) { 
            delay(200); 
            digitalWrite(led1, LOW); 
            digitalWrite(led2, HIGH); 
            LEDstatus = HIGH;
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("SECURITY : ON");
            int colorR= 0;
            int colorG = 255;
            lcd.setRGB(colorR, colorG, colorB); 
            }
           else {
            delay(200); 
            digitalWrite(led1, HIGH);
            digitalWrite(led2, LOW);   
            LEDstatus = LOW; 
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("SECURITY : OFF");
            int colorR= 255;
            int colorG = 0;
            lcd.setRGB(colorR, colorG, colorB); 
            }
       }
       
       oldSwitchState = newSwitchState;
        }

        
}



//////////////////////////////////////////////////


void setup() {
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setRGB(colorR, colorG, colorB);
  lcd.print("SECURITY :OFF");

  pinMode(led1, OUTPUT);
  digitalWrite(led1, HIGH);
  pinMode(led2, OUTPUT);
  pinMode(but, INPUT);

}

void loop() {
  
  monSysteme();

}
