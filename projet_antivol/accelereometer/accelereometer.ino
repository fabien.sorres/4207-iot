const int led1 = 2;
const int led2 = 7;

#include <Wire.h>
#include "MMA7660.h"
MMA7660 accelemeter;

void setup() {
  // put your setup code here, to run once:
  accelemeter.init();
  Serial.begin(9600);

  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  float ax, ay, az;

  accelemeter.getAcceleration(&ax, &ay, &az);
  Serial.println("accleration of X/Y/Z: ");
  Serial.print(ax);
  Serial.println(" g");
  Serial.print(ay);
  Serial.println(" g");
  Serial.print(az);
  Serial.println(" g");
  Serial.println("***********");
  if ( ax > 0.5 || ay > 0.5 || az > 0.5) {

    digitalWrite(led1, HIGH);
    digitalWrite(led2, LOW);
    delay(200);
    digitalWrite(led1, LOW);
    digitalWrite(led2, HIGH);
    delay(200);
  }
  else {
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
  }

}
