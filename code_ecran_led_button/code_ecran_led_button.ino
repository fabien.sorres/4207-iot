#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;

const int colorR = 255;
const int colorG = 0;
const int colorB = 0;
const int led = 3;

void setup()
{

  pinMode(led, OUTPUT);
  pinMode(A0, INPUT);
  lcd.begin(16, 2);
  lcd.setRGB(colorR, colorG, colorB);
  lcd.print("");
  Serial.begin(9600);
  
}

void loop()
{
  
  if(analogRead(A0)!=0) {
    digitalWrite(led, HIGH);
    }
  else {
    digitalWrite(led, LOW);
    }
  
}
